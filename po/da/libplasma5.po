# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2014, 2015, 2016, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-21 00:42+0000\n"
"PO-Revision-Date: 2019-11-26 19:29+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: declarativeimports/calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Mærkedage"

#: declarativeimports/calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Begivenheder"

#: declarativeimports/calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Gøremål"

#: declarativeimports/calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Andet"

#: declarativeimports/calendar/qml/MonthView.qml:244
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr ""

#: declarativeimports/calendar/qml/MonthView.qml:255
#, kde-format
msgid "Previous Month"
msgstr "Forrige måned"

#: declarativeimports/calendar/qml/MonthView.qml:257
#, kde-format
msgid "Previous Year"
msgstr "Forrige år"

#: declarativeimports/calendar/qml/MonthView.qml:259
#, kde-format
msgid "Previous Decade"
msgstr "Forrige årti"

#: declarativeimports/calendar/qml/MonthView.qml:272
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "I dag"

#: declarativeimports/calendar/qml/MonthView.qml:273
#, kde-format
msgid "Reset calendar to today"
msgstr "Nulstil kalender til i dag"

#: declarativeimports/calendar/qml/MonthView.qml:282
#, kde-format
msgid "Next Month"
msgstr "Næste måned"

#: declarativeimports/calendar/qml/MonthView.qml:284
#, kde-format
msgid "Next Year"
msgstr "Næste år"

#: declarativeimports/calendar/qml/MonthView.qml:286
#, kde-format
msgid "Next Decade"
msgstr "Næste årti"

#: declarativeimports/calendar/qml/MonthView.qml:306
#, kde-format
msgid "Days"
msgstr ""

#: declarativeimports/calendar/qml/MonthView.qml:311
#, fuzzy, kde-format
#| msgid "Next Month"
msgid "Months"
msgstr "Næste måned"

#: declarativeimports/calendar/qml/MonthView.qml:316
#, kde-format
msgid "Years"
msgstr ""

#: declarativeimports/plasmacomponents/qml/QueryDialog.qml:21
#, kde-format
msgid "OK"
msgstr "OK"

#: declarativeimports/plasmacomponents/qml/QueryDialog.qml:22
#, kde-format
msgid "Cancel"
msgstr "Annullér"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:80
#, kde-format
msgid "More actions"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:559
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:559
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:46
#, kde-format
msgid "Password"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:66
#, kde-format
msgid "Search…"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:68
#, kde-format
msgid "Search"
msgstr ""

#: plasma/applet.cpp:379
#, kde-format
msgid "Unknown"
msgstr "Ukendt"

#: plasma/applet.cpp:759
#, kde-format
msgid "Activate %1 Widget"
msgstr "Aktivér widgetten %1"

#: plasma/containment.cpp:115 plasma/private/applet_p.cpp:106
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr "Fjern %1"

#: plasma/containment.cpp:121 plasma/corona.cpp:407 plasma/corona.cpp:497
#, kde-format
msgid "Enter Edit Mode"
msgstr ""

#: plasma/containment.cpp:124 plasma/private/applet_p.cpp:111
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr "Indstil %1..."

#: plasma/corona.cpp:356 plasma/corona.cpp:483
#, kde-format
msgid "Lock Widgets"
msgstr "Lås widgets"

#: plasma/corona.cpp:356
#, kde-format
msgid "Unlock Widgets"
msgstr "Lås widgets op"

#: plasma/corona.cpp:405
#, kde-format
msgid "Exit Edit Mode"
msgstr ""

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr "Om der skal oprettes en cache på disken til temaet."

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"Den maksimale størrelse på tema-cachen på disken i kilobyte. Bemærk at disse "
"filer er små, så den maksimale størrelse bliver måske ikke brugt. Det er "
"altså normalt helt ufarligt at angive en stor størrelse."

#: plasma/packagestructure/packages.cpp:28
#: plasma/packagestructure/packages.cpp:47
#, kde-format
msgid "Main Script File"
msgstr "Hovedscriptfil"

#: plasma/packagestructure/packages.cpp:29
#, kde-format
msgid "Tests"
msgstr "Tester"

#: plasma/packagestructure/packages.cpp:69
#, kde-format
msgid "Images"
msgstr "Billeder"

#: plasma/packagestructure/packages.cpp:70
#, kde-format
msgid "Themed Images"
msgstr "Tematiserede billeder"

#: plasma/packagestructure/packages.cpp:76
#, kde-format
msgid "Configuration Definitions"
msgstr "Indstillingsdefinitioner"

#: plasma/packagestructure/packages.cpp:81
#, kde-format
msgid "User Interface"
msgstr "Brugerflade"

#: plasma/packagestructure/packages.cpp:83
#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:29
#, kde-format
msgid "Data Files"
msgstr "Datafiler"

#: plasma/packagestructure/packages.cpp:85
#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:31
#, kde-format
msgid "Executable Scripts"
msgstr "Kørbare scripts"

#: plasma/packagestructure/packages.cpp:89
#, kde-format
msgid "Screenshot"
msgstr "Skærmbillede"

#: plasma/packagestructure/packages.cpp:91
#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:39
#, kde-format
msgid "Translations"
msgstr "Oversættelser"

#: plasma/packagestructure/plasma_applet_packagestructure.cpp:30
#, kde-format
msgid "Configuration UI pages model"
msgstr "Sidemodel til konfigurationsbrugerflade"

#: plasma/packagestructure/plasma_applet_packagestructure.cpp:31
#, kde-format
msgid "Configuration XML file"
msgstr "XML-konfigurationsfil"

#: plasma/packagestructure/plasma_applet_packagestructure.cpp:47
#, kde-format
msgid "Custom expander for compact applets"
msgstr "Brugertilpasset udvider til kompakte applets"

#: plasma/packagestructure/plasma_dataengine_packagestructure.cpp:36
#, kde-format
msgid "Service Descriptions"
msgstr "Tjenestebeskrivelser"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:30
#, kde-format
msgid "Images for dialogs"
msgstr "Billeder til dialoger"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:31
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:32
#, kde-format
msgid "Generic dialog background"
msgstr "Generisk dialogbaggrund"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:33
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:34
#, kde-format
msgid "Theme for the logout dialog"
msgstr "Tema til log ud-dialog"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:36
#, kde-format
msgid "Wallpaper packages"
msgstr "Baggrundsbillede-pakker"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:38
#, kde-format
msgid "Images for widgets"
msgstr "Billeder til widgets"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:39
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:40
#, kde-format
msgid "Background image for widgets"
msgstr "Baggrundsbillede til widgets"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:41
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:42
#, kde-format
msgid "Analog clock face"
msgstr "Analog urplade"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:43
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:44
#, kde-format
msgid "Background image for panels"
msgstr "Baggrundsbillede til paneler"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:45
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:46
#, kde-format
msgid "Background for graphing widgets"
msgstr "Baggrund til graf-widgets"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:47
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:48
#, kde-format
msgid "Background image for tooltips"
msgstr "Baggrundsbillede til værktøjstips"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:50
#, kde-format
msgid "Opaque images for dialogs"
msgstr "Ugennemsigtige billeder til dialoger"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:51
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:52
#, kde-format
msgid "Opaque generic dialog background"
msgstr "Ugennemsigtig generisk dialogbaggrund"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:55
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:58
#, kde-format
msgid "Opaque theme for the logout dialog"
msgstr "Ugennemsigtigt tema til log ud-dialogen"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:60
#, kde-format
msgid "Opaque images for widgets"
msgstr "Ugennemsigtige billeder til widgets"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:63
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:66
#, kde-format
msgid "Opaque background image for panels"
msgstr "Ugennemsigtigt baggrundsbillede til paneler"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:67
#: plasma/packagestructure/plasma_theme_packagestructure.cpp:68
#, kde-format
msgid "Opaque background image for tooltips"
msgstr "Ugennemsigtigt baggrundsbillede til værktøjstips"

#: plasma/packagestructure/plasma_theme_packagestructure.cpp:70
#, kde-format
msgid "KColorScheme configuration file"
msgstr "Konfigurationsfil til KColorScheme"

#: plasma/pluginloader.cpp:593 plasma/pluginloader.cpp:594
#, kde-format
msgctxt "misc category"
msgid "Miscellaneous"
msgstr "Diverse"

#: plasma/private/applet_p.cpp:126
#, kde-format
msgid "The %1 widget did not define which ScriptEngine to use."
msgstr "Widgetten %1 definerede ikke hvilken scriptmotor der skal bruges."

#: plasma/private/applet_p.cpp:145
#, kde-format
msgctxt "Package file, name of the widget"
msgid "Could not open the %1 package required for the %2 widget."
msgstr "Kunne ikke åbne pakken %1 som er påkrævet af widgetten %2."

#: plasma/private/applet_p.cpp:166
#, kde-format
msgctxt ""
"API or programming language the widget was written in, name of the widget"
msgid "Could not create a %1 ScriptEngine for the %2 widget."
msgstr "Kunne ikke oprette en %1-ScriptEngine til widgetten %2."

#: plasma/private/applet_p.cpp:172
#, kde-format
msgid "Show Alternatives..."
msgstr "Vis alternativer..."

#: plasma/private/applet_p.cpp:268
#, kde-format
msgid "Widget Removed"
msgstr "Widget fjernet"

#: plasma/private/applet_p.cpp:269
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "Widgetten \"%1\" er blevet fjernet."

#: plasma/private/applet_p.cpp:273
#, kde-format
msgid "Panel Removed"
msgstr "Panel fjernet"

#: plasma/private/applet_p.cpp:274
#, kde-format
msgid "A panel has been removed."
msgstr "Et panel er blevet fjernet."

#: plasma/private/applet_p.cpp:277
#, kde-format
msgid "Desktop Removed"
msgstr "Skrivebord fjernet"

#: plasma/private/applet_p.cpp:278
#, kde-format
msgid "A desktop has been removed."
msgstr "Et skrivebord er blevet fjernet."

#: plasma/private/applet_p.cpp:281
#, kde-format
msgid "Undo"
msgstr "Fortryd"

#: plasma/private/applet_p.cpp:369
#, kde-format
msgid "Widget Settings"
msgstr "Indstilling af widget"

#: plasma/private/applet_p.cpp:376
#, kde-format
msgid "Remove this Widget"
msgstr "Fjern denne widget"

#: plasma/private/applet_p.cpp:383
#: plasma/private/associatedapplicationmanager.cpp:65
#: plasma/private/associatedapplicationmanager.cpp:124
#: plasma/private/associatedapplicationmanager.cpp:166
#, kde-format
msgid "Run the Associated Application"
msgstr "Kør det associerede program"

#: plasma/private/applet_p.cpp:474
#, kde-format
msgid "Script initialization failed"
msgstr "Initialisering af script mislykkedes"

#: plasma/private/associatedapplicationmanager.cpp:62
#: plasma/private/associatedapplicationmanager.cpp:163
#, kde-format
msgid "Open with %1"
msgstr "Åbn med %1"

#: plasma/private/containment_p.cpp:58
#, kde-format
msgid "Remove this Panel"
msgstr "Fjern dette panel"

#: plasma/private/containment_p.cpp:60
#, kde-format
msgid "Remove this Activity"
msgstr "Fjern denne aktivitet"

#: plasma/private/containment_p.cpp:66
#, kde-format
msgid "Activity Settings"
msgstr "Aktivitetsindstillinger"

#: plasma/private/containment_p.cpp:72
#, kde-format
msgid "Add Widgets..."
msgstr "Tilføj widgets..."

#: plasma/private/containment_p.cpp:188
#, kde-format
msgid "Could not find requested component: %1"
msgstr "Kunne ikke finde den anmodede komponent: %1"

#. i18n: ectx: property (text), widget (QLabel, label)
#: plasma/private/publish.ui:17
#, kde-format
msgid ""
"Sharing a widget on the network allows you to access this widget from "
"another computer as a remote control."
msgstr ""
"Deling af en widget på netværket lader dig tilgå denne widget fra en anden "
"computer som fjernbetjening."

#. i18n: ectx: property (text), widget (QCheckBox, publishCheckbox)
#: plasma/private/publish.ui:27
#, kde-format
msgid "Share this widget on the network"
msgstr "Del denne widget på netværket"

#. i18n: ectx: property (text), widget (QCheckBox, allUsersCheckbox)
#: plasma/private/publish.ui:37
#, kde-format
msgid "Allow everybody to freely access this widget"
msgstr "Lad alle få fri adgang til denne widget"

#: plasma/private/service_p.h:32
#, kde-format
msgctxt "Error message, tried to start an invalid service"
msgid "Invalid (null) service, can not perform any operations."
msgstr "Ugyldig (null) tjeneste, kan ikke udføre nogen handlinger."

#: plasmaquick/appletquickitem.cpp:604
#, fuzzy, kde-format
#| msgid "Unknown"
msgid "Unknown Applet"
msgstr "Ukendt"

#: plasmaquick/appletquickitem.cpp:619
#, fuzzy, kde-format
#| msgid "Error loading QML file: %1"
msgid "Error loading QML file: %1 %2"
msgstr "Fejl ved indlæsning af QML-fil: %1"

#: plasmaquick/appletquickitem.cpp:621
#, kde-format
msgid "Error loading Applet: package inexistent. %1"
msgstr "Fejl ved indlæsning af applet. Pakken eksisterer ikke. %1"

#: plasmaquick/configview.cpp:95
#, kde-format
msgid "%1 Settings"
msgstr "Indstilling af %1"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:593
#, kde-format
msgid "Plasma Package"
msgstr "Plasma-pakke"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:597
#, kde-format
msgid "Install"
msgstr "Installér"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:610
#, kde-format
msgid "Package Installation Failed"
msgstr "Installation af pakken mislykkedes"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:633
#, kde-format
msgid "The package you just dropped is invalid."
msgstr "Pakken du netop slap er ugyldig."

#: scriptengines/qml/plasmoid/containmentinterface.cpp:642
#: scriptengines/qml/plasmoid/containmentinterface.cpp:711
#, kde-format
msgid "Widgets"
msgstr "Widgets"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:647
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr "Tilføj %1"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:661
#: scriptengines/qml/plasmoid/containmentinterface.cpp:715
#, kde-format
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr "Tilføj ikon"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:673
#, kde-format
msgid "Wallpaper"
msgstr "Baggrundsbillede"

#: scriptengines/qml/plasmoid/containmentinterface.cpp:683
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr "Sæt %1"

#: scriptengines/qml/plasmoid/dropmenu.cpp:28
#, kde-format
msgid "Content dropped"
msgstr "Indhold droppet"

#~ msgid "Accessibility"
#~ msgstr "Tilgængelighed"

#~ msgid "Application Launchers"
#~ msgstr "Startmenuer"

#~ msgid "Astronomy"
#~ msgstr "Astronomi"

#~ msgid "Date and Time"
#~ msgstr "Dato og klokkeslæt"

#~ msgid "Development Tools"
#~ msgstr "Udviklingsværktøjer"

#~ msgid "Education"
#~ msgstr "Uddannelse"

#~ msgid "Environment and Weather"
#~ msgstr "Miljø og vejr"

#~ msgid "Examples"
#~ msgstr "Eksempler"

#~ msgid "File System"
#~ msgstr "Filsystem"

#~ msgid "Fun and Games"
#~ msgstr "Spil og spas"

#~ msgid "Graphics"
#~ msgstr "Grafik"

#~ msgid "Language"
#~ msgstr "Sprog"

#~ msgid "Mapping"
#~ msgstr "Kort"

#~ msgid "Miscellaneous"
#~ msgstr "Diverse"

#~ msgid "Multimedia"
#~ msgstr "Multimedie"

#~ msgid "Online Services"
#~ msgstr "Online tjenester"

#~ msgid "Productivity"
#~ msgstr "Produktivitet"

#~ msgid "System Information"
#~ msgstr "Systeminformation"

#~ msgid "Utilities"
#~ msgstr "Værktøjer"

#~ msgid "Windows and Tasks"
#~ msgstr "Vinduer og opgaver"

#~ msgid "Clipboard"
#~ msgstr "Udklipsholder"

#~ msgid "Tasks"
#~ msgstr "Opgaver"

#~ msgctxt "%1 is the name of the containment"
#~ msgid "Edit %1..."
#~ msgstr "Redigér %1..."

#~ msgid "Default settings for theme, etc."
#~ msgstr "Standardindstillinger for tema osv."

#~ msgid "Color scheme to use for applications."
#~ msgstr "Farveskema som skal bruges til programmer."

#~ msgid "Preview Images"
#~ msgstr "Forhåndsvis billeder"

#~ msgid "Preview for the Login Manager"
#~ msgstr "Forhåndsvisning af login-håndteringen"

#~ msgid "Preview for the Lock Screen"
#~ msgstr "Forhåndsvisning af låseskærmen"

#~ msgid "Preview for the Userswitcher"
#~ msgstr "Forhåndsvisning af brugerskifter"

#~ msgid "Preview for the Virtual Desktop Switcher"
#~ msgstr "Forhåndsvisning af virtuel skrivebordsvælger"

#~ msgid "Preview for Splash Screen"
#~ msgstr "Forhåndsvisning af opstartsskærm"

#~ msgid "Preview for KRunner"
#~ msgstr "Forhåndsvisning af KRunner"

#~ msgid "Preview for the Window Decorations"
#~ msgstr "Forhåndsvisning af vinduesdekorationer"

#~ msgid "Preview for Window Switcher"
#~ msgstr "Forhåndsvisning af vinduesskifter"

#~ msgid "Login Manager"
#~ msgstr "Login-håndtering"

#~ msgid "Main Script for Login Manager"
#~ msgstr "Hovedscript til login-håndtering"

#~ msgid "Logout Dialog"
#~ msgstr "Log ud-dialog"

#~ msgid "Main Script for Logout Dialog"
#~ msgstr "Hovedscript til log ud-dialog"

#~ msgid "Screenlocker"
#~ msgstr "Skærmlås"

#~ msgid "Main Script for Lock Screen"
#~ msgstr "Hovedscript til låseskærm"

#~ msgid "UI for fast user switching"
#~ msgstr "Brugerflade til hurtigt brugerskift"

#~ msgid "Main Script for User Switcher"
#~ msgstr "Hovedscript til brugerskifter"

#~ msgid "Virtual Desktop Switcher"
#~ msgstr "Skift mellem virtuelle skriveborde"

#~ msgid "Main Script for Virtual Desktop Switcher"
#~ msgstr "Hovedscript til virtuel skrivebordsvælger"

#~ msgid "On-Screen Display Notifications"
#~ msgstr "Bekendtgørelser med on-screen-display"

#~ msgid "Main Script for On-Screen Display Notifications"
#~ msgstr "Hovedscript til bekendtgørelser med on-screen-display"

#~ msgid "Splash Screen"
#~ msgstr "Opstartsskærm"

#~ msgid "Main Script for Splash Screen"
#~ msgstr "Hovedscript til opstartsskærm"

#~ msgid "KRunner UI"
#~ msgstr "KRunner-brugerflade"

#~ msgid "Main Script KRunner"
#~ msgstr "Hovedscript til KRunner"

#~ msgid "Window Decoration"
#~ msgstr "Vinduesdekoration"

#~ msgid "Main Script for Window Decoration"
#~ msgstr "Hovedscript til vinduesdekoration"

#~ msgid "Window Switcher"
#~ msgstr "Vinduesskifter"

#~ msgid "Main Script for Window Switcher"
#~ msgstr "Hovedscript til vinduesskifter"

#~ msgid "Finish Customizing Layout"
#~ msgstr "Afslut tilpasning af layout"

#~ msgid "Customize Layout..."
#~ msgstr "Tilpas layout..."

#~ msgid "Fetching file type..."
#~ msgstr "Henter filtype..."

#~ msgctxt "%1 is the name of the containment"
#~ msgid "%1 Options"
#~ msgstr "Indstilling af %1"

#~ msgctxt "%1 is the name of the applet"
#~ msgid "Remove this %1"
#~ msgstr "Fjern denne %1"

#~ msgctxt "%1 is the name of the applet"
#~ msgid "%1 Settings"
#~ msgstr "Indstilling af %1"

#~ msgctxt "%1 is the name of the applet"
#~ msgid "%1 Settings..."
#~ msgstr "Indstilling af %1..."

#~ msgid "Low color images for dialogs"
#~ msgstr "Billeder med lav farve til dialoger"

#~ msgid "Low color generic dialog background"
#~ msgstr "Generisk dialogbaggrund med lav farve"

#~ msgid "Low color theme for the logout dialog"
#~ msgstr "Tema til log ud-dialog med lav farve"

#~ msgid "Low color background image for widgets"
#~ msgstr "Baggrundsbillede til widgets med lav farve"

#~ msgid "Low color analog clock face"
#~ msgstr "Front til analogt ur med lav farve"

#~ msgid "Low color background image for panels"
#~ msgstr "Baggrundsbillede til paneler med lav farve"

#~ msgid "Low color background for graphing widgets"
#~ msgstr "Baggrund til graf-widgets med lav farve"

#~ msgid "Low color background image for tooltips"
#~ msgstr "Baggrundsbillede til værktøjstips med lav farve"

#~ msgid "Plasma Package Manager"
#~ msgstr "Plasma pakkehåndtering"

#~ msgctxt "Do not translate <path>"
#~ msgid "Generate a SHA1 hash for the package at <path>"
#~ msgstr "Generér en SHA1-hash for pakken i <path>"

#~ msgid "For install or remove, operates on packages installed for all users."
#~ msgstr ""
#~ "Til at installere eller fjerne. Påvirker pakker installeret for alle "
#~ "brugere."

#~ msgctxt ""
#~ "theme, wallpaper, etc. are keywords, but they may be translated, as both "
#~ "versions are recognized by the application (if translated, should be same "
#~ "as messages with 'package type' context below)"
#~ msgid ""
#~ "The type of package, e.g. theme, wallpaper, plasmoid, dataengine, runner, "
#~ "layout-template, etc."
#~ msgstr ""
#~ "Typen af pakke, f.eks. tema, baggrundsbillede, plasmoid, datamotor, "
#~ "runner, layout-skabelon, osv."

#~ msgctxt "Do not translate <path>"
#~ msgid "Install the package at <path>"
#~ msgstr "Installér pakken i <path>"

#~ msgctxt "Do not translate <name>"
#~ msgid "Show information of package <name>"
#~ msgstr "Vis information om pakken <name>"

#~ msgctxt "Do not translate <path>"
#~ msgid "Upgrade the package at <path>"
#~ msgstr "Opgradér pakken i <path>"

#~ msgid "List installed packages"
#~ msgstr "Liste over installerede pakker"

#~ msgid "List all known package types that can be installed"
#~ msgstr "Oplist alle kendte pakketyper som kan installeres"

#~ msgctxt "Do not translate <name>"
#~ msgid "Remove the package named <name>"
#~ msgstr "Fjern pakken kaldet <name>"

#~ msgid ""
#~ "Absolute path to the package root. If not supplied, then the standard "
#~ "data directories for this KDE session will be searched instead."
#~ msgstr ""
#~ "Absolut sti til pakkens rod. Hvis ikke angivet, vil standard datamapper "
#~ "for denne KDE-session blive gennemsøgt i stedet."

#~ msgid "Failed to generate a Package hash for %1"
#~ msgstr "Kunne ikke generere pakke-hash for %1"

#~ msgid "SHA1 hash for Package at %1: '%2'"
#~ msgstr "SHA1-hash for pakken i %1: \"%2\""

#~ msgctxt "package type"
#~ msgid "wallpaper"
#~ msgstr "baggrundsbillede"

#~ msgctxt "package type"
#~ msgid "plasmoid"
#~ msgstr "plasmoid"

#~ msgctxt "package type"
#~ msgid "package"
#~ msgstr "pakke"

#~ msgctxt "package type"
#~ msgid "theme"
#~ msgstr "tema"

#~ msgctxt "package type"
#~ msgid "dataengine"
#~ msgstr "datamotor"

#~ msgctxt "package type"
#~ msgid "runner"
#~ msgstr "runner"

#~ msgctxt "package type"
#~ msgid "wallpaperplugin"
#~ msgstr "baggrundsbillede-plugin"

#~ msgctxt "package type"
#~ msgid "lookandfeel"
#~ msgstr "udseende og fremtoning"

#~ msgctxt "package type"
#~ msgid "shell"
#~ msgstr "skal"

#~ msgctxt "package type"
#~ msgid "layout-template"
#~ msgstr "layout-skabelon"

#~ msgctxt "package type"
#~ msgid "kwineffect"
#~ msgstr "kwin-effekt"

#~ msgctxt "package type"
#~ msgid "windowswitcher"
#~ msgstr "vinduesskifter"

#~ msgctxt "package type"
#~ msgid "kwinscript"
#~ msgstr "kwin-script"

#~ msgid "Could not find a suitable installer for package of type %1"
#~ msgstr ""
#~ "Kunne ikke finde passende installationsprogram til pakke af typen %1"

#~ msgid "Listing service types: %1"
#~ msgstr "Oplister tjenestetyper: %1"

#~ msgid "Error: Plugin %1 is not installed."
#~ msgstr "Fejl: Plugin %1 er ikke installeret."

#~ msgctxt ""
#~ "No option was given, this is the error message telling the user he needs "
#~ "at least one, do not translate install, remove, upgrade nor list"
#~ msgid "One of install, remove, upgrade or list is required."
#~ msgstr "Enten installér, fjern, opgradér eller vis liste er påkrævet."

#~ msgid "Error: Can't find plugin metadata: %1"
#~ msgstr "Fejl: Kan ikke finde plugin-metadata: %1"

#~ msgid "Showing info for package: %1"
#~ msgstr "Viser info for pakken: %1"

#~ msgid "      Name : %1"
#~ msgstr "      Navn : %1"

#~ msgid "   Comment : %1"
#~ msgstr "   Kommentar: %1"

#~ msgid "    Plugin : %1"
#~ msgstr "    Plugin : %1"

#~ msgid "    Author : %1"
#~ msgstr "    Ophavsmand : %1"

#~ msgid "      Path : %1"
#~ msgstr "      Sti: %1"

#~ msgctxt ""
#~ "The user entered conflicting options packageroot and global, this is the "
#~ "error message telling the user he can use only one"
#~ msgid ""
#~ "The packageroot and global options conflict each other, please select "
#~ "only one."
#~ msgstr ""
#~ "Pakkeroden og de globale indstillinger er i modstrid med hinanden, vælg "
#~ "en anden."

#~ msgid "Addon Name"
#~ msgstr "Navn på tilføjelse"

#~ msgid "Service Type"
#~ msgstr "Tjenestetype"

#~ msgid "Path"
#~ msgstr "Sti"

#~ msgid "Type Argument"
#~ msgstr "Typeargument"

#~ msgid "Package types that are installable with this tool:"
#~ msgstr "Pakketyper der kan installeres med dette værktøj:"

#~ msgid "Built in:"
#~ msgstr "Indbygget:"

#~ msgid "DataEngine"
#~ msgstr "Datamotor"

#~ msgid "Layout Template"
#~ msgstr "Layout-skabelon"

#~ msgid "Look and Feel"
#~ msgstr "Udseende og fremtoning"

#~ msgid "Package"
#~ msgstr "Pakke"

#~ msgid "Plasmoid"
#~ msgstr "Plasmoid"

#~ msgid "Runner"
#~ msgstr "Runner"

#~ msgid "Shell"
#~ msgstr "Skal"

#~ msgid "Theme"
#~ msgstr "Tema"

#~ msgid "Wallpaper Images"
#~ msgstr "Baggrundsbilleder"

#~ msgid "Animated Wallpaper"
#~ msgstr "Animeret baggrundsbillede"

#~ msgid "KWin Effect"
#~ msgstr "KWin-effekt"

#~ msgid "KWin Window Switcher"
#~ msgstr "KWin vinduesskifter"

#~ msgid "KWin Script"
#~ msgstr "KWin-script"

#~ msgid "Provided by plugins:"
#~ msgstr "Leveret af plugins:"

#~ msgid "Provided by .desktop files:"
#~ msgstr "Leveret af .desktop-filer:"

#~ msgid "Successfully upgraded %1"
#~ msgstr "Opgradering af %1 gennemført"

#~ msgid "Successfully installed %1"
#~ msgstr "Installation af %1 gennemført"

#~ msgid "Error: Installation of %1 failed: %2"
#~ msgstr "Fejl: Installation af %1 mislykkedes: %2"

#~ msgid "Upgrading package from file: %1"
#~ msgstr "Opgraderer pakke fra fil: %1"

#~ msgid "Successfully uninstalled %1"
#~ msgstr "Afinstallation af %1 gennemført"

#~ msgid "Error: Uninstallation of %1 failed: %2"
#~ msgstr "Fejl: Afinstallation af %1 mislykkedes: %2"

#~ msgid ""
#~ "Could not load installer for package of type %1. Error reported was: %2"
#~ msgstr ""
#~ "Kunne ikke indlæse installationsprogram til pakketypen %1. Fejlbeskeden "
#~ "var: %2"

#~ msgctxt "%1 is the name of the containment"
#~ msgid "Do you really want to remove this %1?"
#~ msgstr "Vil du fjerne %1?"
